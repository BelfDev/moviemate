//
//  FavoritesHelper.swift
//  moviemate
//
//  Created by Pedro Fernandes on 12/14/17.
//  Copyright © 2017 Pedro Henrique Belfort Fernandes. All rights reserved.
//

import Foundation

class FavoritesHelper {
    
    static let favoritesKey = "FAVORITOS"
    
    public static func getFavoriteMovies() -> [Movie] {
        var favoriteMovies = [Movie]()
        if let decoded  = UserDefaults.standard.value(forKey: favoritesKey) as? Data,
            let favorites = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [Movie] {
            favoriteMovies += favorites
        }
        return favoriteMovies
    }
    
    public static func saveFavoriteMovies(_ favoriteMovies: [Movie]) {
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: favoriteMovies)
        UserDefaults.standard.set(encodedData, forKey: favoritesKey)
        UserDefaults.standard.synchronize()
    }
    
    public static func clearFavoriteMovies() {
        UserDefaults.standard.set(nil, forKey: favoritesKey)
        UserDefaults.standard.synchronize()
    }
    
    public static func verifyFavoritesStatus(movie: Movie) -> Bool {
        let favoriteMovies = getFavoriteMovies()
        for favoriteMovie in favoriteMovies {
            if (movie.id == favoriteMovie.id) {
                return movie.id == favoriteMovie.id
            }
        }
        return false
    }
}
