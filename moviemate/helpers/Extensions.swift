//
//  extensions.swift
//  moviemate
//
//  Created by Pedro Fernandes on 12/14/17.
//  Copyright © 2017 Pedro Henrique Belfort Fernandes. All rights reserved.
//

import UIKit
import Foundation

extension UIImageView {
    public func image(fromUrl urlString: String) {
        guard let url = URL(string: urlString) else {
            print("Couldn't create URL from \(urlString)")
            return
        }
        let theTask = URLSession.shared.dataTask(with: url) {
            data, response, error in
            if let response = data {
                DispatchQueue.main.async {
                    self.image = UIImage(data: response)
                }
            }
        }
        theTask.resume()
    }
}
