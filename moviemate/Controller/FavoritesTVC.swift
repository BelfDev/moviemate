//
//  FavoritesTVC.swift
//  moviemate
//
//  Created by Pedro Fernandes on 12/12/17.
//  Copyright © 2017 Pedro Henrique Belfort Fernandes. All rights reserved.
//

import UIKit

class FavoritesTVC: BaseTVC, MovieCellDelegate {
    
    // MARK: - Lifecycle
    
    override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            fetchFavorites()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return super.numberOfSections(in: tableView)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return super.tableView(tableView, numberOfRowsInSection: section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath) as! MovieCell
        cell.delegate = self
        cell.favoriteButton.setImage(#imageLiteral(resourceName: "star_full"), for: .normal)
        return cell
    }
    
    // MARK: - Favorite Operations
    
    fileprivate func fetchFavorites() -> Void {
        self.movieList = FavoritesHelper.getFavoriteMovies()
        self.tableView.reloadData()
    }
    
    // MARK: - Cell Operations
    
    internal func didSelectFavoriteButton(in cell: MovieCell) {
        if let indexPath = self.tableView.indexPath(for: cell) {
            
            let favoriteMovies = FavoritesHelper.getFavoriteMovies()
            let movie = favoriteMovies[indexPath.row]
            
            movie.setFavorite(false)
            BaseTVC.addToFavorites(movie)
            
            self.movieList.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
}
