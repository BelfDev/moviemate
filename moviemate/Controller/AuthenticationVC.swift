//
//  AuthenticationVC.swift
//  moviemate
//
//  Created by Pedro Fernandes on 12/12/17.
//  Copyright © 2017 Pedro Henrique Belfort Fernandes. All rights reserved.
//

import UIKit
import FirebaseAuth

class AuthenticationVC: UIViewController, UITextFieldDelegate {
    
    fileprivate let movieCatalogSegue = "ENTER_MOVIE_CATALOG_SEGUE"
    
    fileprivate let forgotPasswordMode = 100
    fileprivate let loginMode = 200
    fileprivate let signupMode = 300
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    @IBOutlet weak var signupNowButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Auth.auth().addStateDidChangeListener { (auth, user: User?) in
            if user != nil {
                self.performSegue(withIdentifier: self.movieCatalogSegue, sender: nil)
            }
        }
    }
    
    // MARK: - Button Actions
    
    @IBAction func confirmUserAction(_ sender: UIButton) {
        switch sender.tag {
        case signupMode:
            signUserUp()
            break;
        case loginMode:
            signUserIn(email: usernameTextField.text!, password: passwordTextField.text!)
            break;
        default:
            break;
        }
    }
    
    @IBAction func activateSignUpMode(_ sender: Any) {
        setupSignUpMode()
    }
    
    @IBAction func retrievePassword(_ sender: Any) {
        sendForgotPasswordRequest()
    }
    
    @IBAction func invokeMode(_ sender: UIButton) {
        switch sender.tag {
        case forgotPasswordMode:
            break;
        case loginMode:
            setupLoginMode()
            break;
        default:
            break;
        }
    }
    
    // MARK: - Authentication Operations
    
    fileprivate func signUserIn(email: String, password: String) {
        Auth.auth().signIn(withEmail: email, password: password, completion: { (user: User?, error) in
            if error != nil {
                self.createAlert(title: "Oops", message: "Parece que você errou seu email ou senha 💩")
            }
        })
    }
    
    fileprivate func signUserUp() {
        let email = usernameTextField.text
        let password = passwordTextField.text
        
        Auth.auth().createUser(withEmail: email!, password: password!, completion: { (user: User?, error) in
            if error == nil {
                self.createAlert(title: "Uhul!", message: "Cadastro feito com sucesso 😉")
                self.signUserIn(email: email!, password: password!)
                FavoritesHelper.clearFavoriteMovies()
            }else{
                self.createAlert(title: "Oops", message: "Errou ao cadastrar, tente novamente 🤪")
            }
        })
    }
    
    fileprivate func sendForgotPasswordRequest() {
        if (usernameTextField.text != nil) {
            Auth.auth().sendPasswordReset(withEmail: usernameTextField.text!, completion: { (error) in
                if error == nil {
                    print("SUCESSO AO ENVIAR EMAIL DE TROCA DE SENHA")
                    self.createAlert(title: "Pronto!", message: "Agora é só verificar sua caixa de email 🤓")
                }else{
                    print("ERRO AO ENVIAR EMAIL DE TROCA DE SENHA")
                    self.createAlert(title: "Oh não...", message: "Erro ao enviar o email para troca de senha, por favor tente novamente mais tarde 🤭")
                }
            })
        }
    }
    
    // MARK: - UI Operations
    
    fileprivate func setupLoginMode() {
        print("LOGIN MODE")
        titleLabel.text = "LOGIN"
        forgotPasswordButton.setTitle("Esqueceu a senha?", for: .normal)
        forgotPasswordButton.tag = forgotPasswordMode
        confirmPasswordTextField.isHidden = true
        signupNowButton.isHidden = false
        confirmButton.setTitle("LOGIN", for: .normal)
        confirmButton.tag = loginMode
    }
    
    fileprivate func setupSignUpMode() {
        signupNowButton.isHidden = true
        confirmPasswordTextField.isHidden = false
        titleLabel.text = "CADASTRO"
        forgotPasswordButton.setTitle("Voltar ao login", for: .normal)
        confirmButton.tag = signupMode
        confirmButton.setTitle("CADASTRAR", for: .normal)
        forgotPasswordButton.tag = loginMode
    }
    
    // MARK: - Convinience
    
    fileprivate func createAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Keyboard
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
