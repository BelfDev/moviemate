//
//  BaseTVC.swift
//  moviemate
//
//  Created by Pedro Fernandes on 14/12/17.
//  Copyright © 2017 Pedro Henrique Belfort Fernandes. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class BaseTVC: UITableViewController {
    
    let cellIdentifier = "cell"
    
    var refMovies: DatabaseReference!
    var movieList = [Movie]()
    
    // MARK: - Lifecycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refMovies = Database.database().reference().child("movies")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 128.0
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    // MARK: - TableView Data Source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movieList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! MovieCell
        
        let movie: Movie
        
        movie = movieList[indexPath.row]
        
        cell.titleLabel.text = movie.title
        cell.subtitleLabel.text = movie.subtitle
        cell.durationLabel.text = String(describing: movie.duration) + " minutos"
        cell.thumbImageView.image(fromUrl: movie.thumbUrl)
        
        let isFavorite = FavoritesHelper.verifyFavoritesStatus(movie: movie)
        cell.favoriteButton.setImage(isFavorite ? #imageLiteral(resourceName: "star_full") : #imageLiteral(resourceName: "star_empty"), for: .normal)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension;
    }
    
    // MARK: - Favorite Operations
    
    class func addToFavorites(_ movie: Movie) {
        var favoriteMovies = FavoritesHelper.getFavoriteMovies()
        
        if (movie.isFavorite()) {
            favoriteMovies.append(movie)
            print(">>>>> ADDED TO FAVORITES")
        } else {
            favoriteMovies = removeFromFavorites(movie, favoriteMovies)
            print(">>>>> REMOVED FROM FAVORITES")
        }
        
        FavoritesHelper.saveFavoriteMovies(favoriteMovies)
    }
    
    class func removeFromFavorites (_ movie: Movie,_ lastFavoriteMovies: [Movie]) -> [Movie] {
        var favoriteMovies = [Movie]()
        favoriteMovies = lastFavoriteMovies
        
        for (index, favoriteMovie) in favoriteMovies.enumerated() {
            if (favoriteMovie.id == movie.id) {
                favoriteMovies.remove(at: index)
                break;
            }
        }
        
        return favoriteMovies
    }
    
    // MARK: - Authentication Operations
    
    @IBAction func logUserOut(_ sender: UIBarButtonItem) {
        do {
            try Auth.auth().signOut()
            dismiss(animated: true, completion: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
}
