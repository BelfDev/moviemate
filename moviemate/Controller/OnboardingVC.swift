//
//  OnboardingVC.swift
//  moviemate
//
//  Created by Pedro Fernandes on 12/12/17.
//  Copyright © 2017 Pedro Henrique Belfort Fernandes. All rights reserved.
//

import UIKit

class OnboardingVC: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var slideScrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    
    fileprivate let authenticationSegue = "ENTER_AUTHENTICATION_SEGUE"
    fileprivate var slides = [Slide]()
    
    fileprivate var currentPage = 0 {
        didSet {
            updateUIToCurrentState()
        }
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        slideScrollView.delegate = self
        
        self.slides = createSlides()
        setupSlideScrollView(slides: self.slides)
        pageControl.numberOfPages = self.slides.count
        pageControl.currentPage = currentPage
    }
    
    // MARK: - Slide Operations
    
    fileprivate func createSlides() -> [Slide] {
        
        let firstSlide = createSlide(image: #imageLiteral(resourceName: "icon_chair"), slogan: "FILMES", subtitle: "Visualize filmes informações sobre os melhores filmes")
        let secondSlide = createSlide(image: #imageLiteral(resourceName: "icon_movie_star"), slogan: "FAVORITOS", subtitle: "Guarde num cantinho os filmes que mais gosta")
        let thirdSlide = createSlide(image: #imageLiteral(resourceName: "icon_happy_man"), slogan: "0800", subtitle: "Tudo isso totalmente de graça, basta registrar-se!")
        
        return [firstSlide, secondSlide, thirdSlide]
    }
    
    fileprivate func createSlide(image: UIImage, slogan: String, subtitle: String) -> Slide {
        let slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide.iconImageView.image = image
        slide.sloganLabel.text = slogan
        slide.subtitleLabel.text = subtitle
        
        return slide
    }
    
    fileprivate func updateUIToCurrentState() {
        if currentPage == (self.slides.count - 1) {
            nextButton.setTitle("OK", for: .normal)
            skipButton.isHidden = true
        } else {
            nextButton.setTitle("PRÓXIMO", for: .normal)
            skipButton.isHidden = false
        }
    }
    
    // MARK: - ScrollView
    
    fileprivate func setupSlideScrollView(slides: [Slide]) {
        
        slideScrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        slideScrollView.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: view.frame.width)
        
        for index in 0..<slides.count {
            slides[index].frame = CGRect(x: view.frame.width * CGFloat(index), y: 0, width: view.frame.width, height: view.frame.height)
            slideScrollView.addSubview(slides[index])
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x / view.frame.width)
        currentPage = Int(pageIndex)
        self.pageControl.currentPage = currentPage
    }
    
    // MARK: - Button Actions
    
    @IBAction func goToNextStep(_ sender: UIButton) {
        
        if (currentPage < self.slides.count - 1) {
            let rectPosition = CGRect(x: 0, y: 0, width: view.frame.width * CGFloat(currentPage + 2), height: view.frame.height)
            slideScrollView.scrollRectToVisible(rectPosition, animated: true)
        } else {
            self.performSegue(withIdentifier: self.authenticationSegue, sender: nil)
        }
    }
    
    @IBAction func skipToAuthentication(_ sender: UIButton) {
        self.performSegue(withIdentifier: self.authenticationSegue, sender: nil)
    }
    
}
