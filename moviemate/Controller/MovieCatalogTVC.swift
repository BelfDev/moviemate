//
//  MovieCatalogTVC.swift
//  moviemate
//
//  Created by Pedro Fernandes on 12/12/17.
//  Copyright © 2017 Pedro Henrique Belfort Fernandes. All rights reserved.
//

import UIKit
import FirebaseDatabase

class MovieCatalogTVC: BaseTVC, MovieCellDelegate {
    
    var activityIndicatorView: UIActivityIndicatorView!
    
    // MARK: - Lifecycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.separatorStyle = .none
        activityIndicatorView.startAnimating()
        fetchMovieData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        tableView.backgroundView = activityIndicatorView
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return super.numberOfSections(in: tableView)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return super.tableView(tableView, numberOfRowsInSection: section)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath) as! MovieCell
        
        cell.delegate = self
        
        return cell
    }
    
    // MARK: - Networking
    
    fileprivate func fetchMovieData() -> Void {
        refMovies.observe(DataEventType.value, with: { (snapshot) in
            
            if snapshot.childrenCount > 0 {
                
                self.movieList.removeAll()
                let favoriteMovies = FavoritesHelper.getFavoriteMovies()
                
                for movie in snapshot.children.allObjects as! [DataSnapshot] {
                    
                    let movieObject = movie.value as? [String: AnyObject]
                    let movieId  = movie.key
                    let movieTitle  = movieObject?["title"]
                    let movieSubtitle = movieObject?["subtitle"]
                    let movieDuration = movieObject?["duration"]
                    let movieSynopsis = movieObject?["synopsis"]
                    let movieThumbUrl = movieObject?["thumb"]
                    
                    let newMovie = Movie(id: movieId as String, title: movieTitle as! String, subtitle: movieSubtitle as! String, duration: movieDuration as! Int, synopsis: movieSynopsis as! String, thumbUrl: movieThumbUrl as! String)
                    
                    for favoriteMovie in favoriteMovies {
                        if (newMovie.id == favoriteMovie.id) {
                            newMovie.setFavorite(true)
                        }
                    }
                    
                    self.movieList.append(newMovie)
                }
                
                self.tableView.separatorStyle = .singleLine
                self.activityIndicatorView.stopAnimating()
                self.tableView.reloadData()
            }
        })
    }
    
    // MARK: - Cell Operations
    
    internal func didSelectFavoriteButton(in cell: MovieCell) {
        if let indexPath = self.tableView.indexPath(for: cell) {
            
            let buttonImage = (cell.favoriteButton.currentImage?.isEqual(#imageLiteral(resourceName: "star_empty")))! ? #imageLiteral(resourceName: "star_full") : #imageLiteral(resourceName: "star_empty")
            
            let movie = self.movieList[indexPath.row]
            movie.setFavorite(movie.isFavorite() ? false : true)
            
            BaseTVC.addToFavorites(movie)
            cell.favoriteButton.setImage(buttonImage, for: .normal)
        }
    }
    
}
