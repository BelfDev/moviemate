//
//  Slide.swift
//  moviemate
//
//  Created by Pedro Fernandes on 15/12/17.
//  Copyright © 2017 Pedro Henrique Belfort Fernandes. All rights reserved.
//

import UIKit

class Slide: UIView {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var sloganLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
}
