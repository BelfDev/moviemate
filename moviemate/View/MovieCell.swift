//
//  MovieCell.swift
//  moviemate
//
//  Created by Pedro Fernandes on 12/12/17.
//  Copyright © 2017 Pedro Henrique Belfort Fernandes. All rights reserved.
//

import UIKit

protocol MovieCellDelegate: class {
    func didSelectFavoriteButton(in cell: MovieCell)
}

class MovieCell: UITableViewCell {

    weak var delegate: MovieCellDelegate?
    
    @IBOutlet weak var thumbImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    
    // TODO: Finish expandable cell implementation
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    @IBAction func didSelectFavoriteButton(_ sender: Any) {
        delegate?.didSelectFavoriteButton(in: self)
    }
    
}
