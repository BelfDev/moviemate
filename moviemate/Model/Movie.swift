//
//  movie.swift
//  moviemate
//
//  Created by Pedro Fernandes on 12/13/17.
//  Copyright © 2017 Pedro Henrique Belfort Fernandes. All rights reserved.
//

import Foundation

class Movie: NSObject, NSCoding {

    var id: String
    let title: String
    let subtitle: String
    let duration: Int
    let synopsis: String
    let thumbUrl: String
    
    var favorite = false
    
    init(id: String, title: String, subtitle: String, duration: Int, synopsis: String, thumbUrl: String) {
        self.id = id
        self.title = title
        self.subtitle = subtitle
        self.duration = duration
        self.synopsis = synopsis
        self.thumbUrl = thumbUrl
    }
    
    func setFavorite (_ isFavorite: Bool) {
        self.favorite = isFavorite
    }
    
    func isFavorite () -> Bool {
        return self.favorite
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: "id") as! String
        self.title = aDecoder.decodeObject(forKey: "title") as! String
        self.subtitle = aDecoder.decodeObject(forKey: "subtitle") as! String
        self.duration = aDecoder.decodeObject(forKey: "duration") as? Int ?? aDecoder.decodeInteger(forKey: "duration")
        self.synopsis = aDecoder.decodeObject(forKey: "synopsis") as! String
        self.thumbUrl = aDecoder.decodeObject(forKey: "thumbUrl") as! String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.title, forKey: "title")
        aCoder.encode(self.subtitle, forKey: "subtitle")
        aCoder.encode(self.duration, forKey: "duration")
        aCoder.encode(self.synopsis, forKey: "synopsis")
        aCoder.encode(self.thumbUrl, forKey: "thumbUrl")
    }
}
