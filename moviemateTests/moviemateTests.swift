//
//  moviemateTests.swift
//  moviemateTests
//
//  Created by Pedro Fernandes on 12/11/17.
//  Copyright © 2017 Pedro Henrique Belfort Fernandes. All rights reserved.
//

import XCTest
@testable import moviemate

class MoviemateTests: XCTestCase {
    
    var movieUnderTest: Movie!
    
    override func setUp() {
        super.setUp()
        movieUnderTest = Movie(id: "5a30343ffc13ae5af2000723", title: "Spring Breakdown", subtitle: "Pre-emptive optimizing attitude", duration: 152, synopsis: "at nibh in hac habitasse platea dictumst aliquam augue quam", thumbUrl: "https://dummyimage.com/244x117.jpg/dddddd/000000")
    }
    
    func testSaveMovieToFavorites() {
        // 1. given
        let updatedFavorites = [Movie(id: "5a30343ffc13ae5af2000723", title: "Spring Breakdown", subtitle: "Pre-emptive optimizing attitude", duration: 152, synopsis: "at nibh in hac habitasse platea dictumst aliquam augue quam", thumbUrl: "https://dummyimage.com/244x117.jpg/dddddd/000000")]

        // 2. when
        FavoritesHelper.saveFavoriteMovies([movieUnderTest])

        // 3. then
        let addedMovie = FavoritesHelper.getFavoriteMovies().first
        XCTAssertEqual(addedMovie?.id, updatedFavorites.first?.id, "Movie could not be saved to Favorites")
    }
    
    func testRemoveMovieFromFavorites() {
        // 1. given
        let updatedFavorites = [Movie]()
        
        // 2. when
        FavoritesHelper.saveFavoriteMovies([movieUnderTest])
        let remainingFavorites = BaseTVC.removeFromFavorites(movieUnderTest, FavoritesHelper.getFavoriteMovies())
        
        // 3. then
        XCTAssertEqual(remainingFavorites, updatedFavorites, "Movie could not be removed from Favorites")
    }
    
    func testVerifyFavoriteStatus() {
        // 1. given
        movieUnderTest.setFavorite(true)
        FavoritesHelper.saveFavoriteMovies([movieUnderTest])
        
        // 2. when
        let movieFavoriteStatus = FavoritesHelper.verifyFavoritesStatus(movie: movieUnderTest)
        
        // 3. then
        XCTAssertEqual(movieFavoriteStatus, true, "Movie favorite status could not be correctly verified")
    }
    
    override func tearDown() {
        movieUnderTest = nil
        FavoritesHelper.clearFavoriteMovies()
        super.tearDown()
    }
    
}
